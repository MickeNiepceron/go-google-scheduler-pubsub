package async

import (
	"cloud.google.com/go/pubsub"
	"context"
	"fmt"
	"go-puller-scheduler/scheduler"
	"log"
	"sync/atomic"
	"time"
)

func PullMessages(projectID string, subID string) error {
	ctx := context.Background()
	client, err := pubsub.NewClient(ctx, projectID)
	if err != nil {
		log.Fatalf("pubsub.NewClient: %v", err)
	}
	defer client.Close()

	subscription := client.Subscription(subID)

	ctx, cancel := context.WithTimeout(ctx, 1*time.Hour)
	defer cancel()

	var received int32
	err = subscription.Receive(ctx, func(_ context.Context, msg *pubsub.Message) {
		fmt.Printf("Received message: %q\n", string(msg.Data))
		atomic.AddInt32(&received, 1)
		msg.Ack()

		scheduler.RunScheduler()
	})
	if err != nil {
		log.Fatalf("sub.Receive: %v", err)
	}

	return nil
}
