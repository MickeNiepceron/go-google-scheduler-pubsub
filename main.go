package main

import (
	"cloud.google.com/go/pubsub"
	"github.com/joho/godotenv"
	"go-puller-scheduler/async"
	"log"
	"os"
)

var client *pubsub.Client

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
	}

	projectID := os.Getenv("GOOGLE_PROJECT_ID")
	subID := os.Getenv("GOOGLE_PUBSUB_SUBSCRIPTION")

	err = async.PullMessages(projectID, subID)
	if err != nil {
		log.Fatal(err)
	}
}
