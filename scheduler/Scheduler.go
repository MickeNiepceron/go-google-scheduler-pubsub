package scheduler

import (
	scheduler "cloud.google.com/go/scheduler/apiv1"
	"context"
	"fmt"
	schedulerpb "google.golang.org/genproto/googleapis/cloud/scheduler/v1"
	"google.golang.org/protobuf/types/known/timestamppb"
	"log"
	"os"
	"time"
)

func RunScheduler() {
	jobName := os.Getenv("GOOGLE_SCHEDULER_JOB_NAME")

	ctx := context.Background()
	schedulerClient, err := scheduler.NewCloudSchedulerClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer schedulerClient.Close()

	req := &schedulerpb.GetJobRequest{
		Name: jobName,
	}
	_, err = schedulerClient.GetJob(ctx, req)
	if err != nil {
		CreateScheduler(ctx, jobName)
	}
}

func CreateScheduler(ctx context.Context, jobName string) {
	topicName := os.Getenv("GOOGLE_PUBSUB_TOPIC")
	resourcePath := os.Getenv("GOOGLE_RESOURCE_PATH")

	schedulerClient, err := scheduler.NewCloudSchedulerClient(ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer schedulerClient.Close()

	schedulerTime := timestamppb.New(time.UnixMilli(60000))

	newJob := &schedulerpb.Job{
		Name: jobName,
		Target: &schedulerpb.Job_PubsubTarget{
			PubsubTarget: &schedulerpb.PubsubTarget{
				TopicName: topicName,
				Data:      []byte("Hello from Google Cloud Scheduler :)))))))))))"),
			},
		},
		Schedule:     "* * * * *", // activate scheduler every minutes
		ScheduleTime: schedulerTime,
	}

	req := &schedulerpb.CreateJobRequest{
		Parent: resourcePath,
		Job:    newJob,
	}

	resp, err := schedulerClient.CreateJob(ctx, req)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(resp) // check job info
}
